package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.zkovalenko.tm.api.service.*;
import ru.t1.zkovalenko.tm.marker.UnitServiceCategory;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

import static ru.t1.zkovalenko.tm.constant.ProjectTestData.PROJECT_NAME;
import static ru.t1.zkovalenko.tm.constant.TaskTestData.TASK_NAME;
import static ru.t1.zkovalenko.tm.constant.UserTestData.USER1;

@Category(UnitServiceCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Nullable
    private Project projectCreated;

    @Nullable
    private Task taskCreated;

    @Nullable
    private Task taskFounded;

    @Before
    public void addProjectTask() {
        projectCreated = projectService.create(USER1.getId(), PROJECT_NAME);
        taskCreated = taskService.create(USER1.getId(), TASK_NAME);
        projectTaskService.bindTaskToProject(USER1.getId(), projectCreated.getId(), taskCreated.getId());
        taskFounded = taskService.findAllByProjectId(USER1.getId(), projectCreated.getId()).get(0);
    }

    @After
    public void after() {
        if (taskCreated != null) taskService.remove(taskCreated);
        taskCreated = null;
        if (projectCreated != null) projectService.remove(projectCreated);
        projectCreated = null;
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertEquals(taskCreated.getId(), taskFounded.getId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertNotNull(taskFounded);
        projectTaskService.removeProjectById(USER1.getId(), projectCreated.getId());
        Assert.assertNull(taskService.findOneById(taskFounded.getId()));
        projectCreated = null;
    }

    @Test
    public void removeProjectByIndex() {
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull Integer index = 1;
        for (Project project : projects) {
            if (project.getId().equals(projectCreated.getId())) break;
            index++;
        }
        projectTaskService.removeProjectByIndex(USER1.getId(), index);
        Assert.assertNull(taskService.findOneById(taskFounded.getId()));
        taskCreated = null;
        projectCreated = null;
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertNotNull(taskFounded);
        projectTaskService.unbindTaskFromProject(USER1.getId(), projectCreated.getId(), taskCreated.getId());
        final int taskSize = taskService.findAllByProjectId(USER1.getId(), projectCreated.getId()).size();
        Assert.assertEquals(0, taskSize);
    }

}
