package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {


    @NotNull String getDatabaseUser();

    @NotNull String getDatabasePassword();

    @NotNull String getDatabaseUrl();

}
