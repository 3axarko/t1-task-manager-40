package ru.t1.zkovalenko.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Update("UPDATE tm_user " +
            "SET login = #{login}, " +
            "   password = #{passwordHash}, " +
            "   email = #{email}, " +
            "   first_name = #{firstName}," +
            "   last_name = #{lastName}," +
            "   middle_name = #{middleName}," +
            "   role = #{role}," +
            "   locked_flg = #{locked}" +
            " WHERE id = #{id}")
    void update(@NotNull User user);

    @Insert("INSERT INTO tm_user " +
            "(id, login, password, email, first_name, last_name, middle_name, role, locked_flg) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void add(@NotNull User user);

    @Select("SELECT * FROM tm_user order by #{sortField}")
    @Results({
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "locked_flg")
    })
    @NotNull List<User> findAll(@NotNull String sortField);

    @Delete("DELETE FROM tm_user")
    void clear();

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results({
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "locked_flg")
    })
    @Nullable User findOneById(@NotNull String id);

    @Select("SELECT * FROM (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM tm_user) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "locked_flg")
    })
    @Nullable User findOneByIndex(@NotNull Integer index);

    @Select("SELECT * FROM tm_user WHERE login = #{login}")
    @Results({
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "locked_flg")
    })
    @Nullable User findByLogin(@NotNull String login);

    @Select("SELECT * FROM tm_user WHERE email = #{email}")
    @Results({
            @Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "locked", column = "locked_flg")
    })
    @Nullable User findByEmail(@NotNull String email);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(@NotNull User user);

}
