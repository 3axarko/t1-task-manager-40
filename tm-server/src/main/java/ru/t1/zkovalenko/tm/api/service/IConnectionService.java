package ru.t1.zkovalenko.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

public interface IConnectionService {

    @NotNull SqlSession openSession();

    @NotNull SqlSessionFactory getSqlSessionFactory();

}
