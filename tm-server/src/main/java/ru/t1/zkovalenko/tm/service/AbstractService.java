package ru.t1.zkovalenko.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.constant.TableConstant;
import ru.t1.zkovalenko.tm.api.repository.IRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IService;
import ru.t1.zkovalenko.tm.comparator.CreatedComparator;
import ru.t1.zkovalenko.tm.comparator.NameComparator;
import ru.t1.zkovalenko.tm.comparator.StatusComparator;
import ru.t1.zkovalenko.tm.enumerated.Sort;
import ru.t1.zkovalenko.tm.exception.entity.EntityNotFoundException;
import ru.t1.zkovalenko.tm.exception.entity.ProjectNotFoundException;
import ru.t1.zkovalenko.tm.exception.field.IdEmptyException;
import ru.t1.zkovalenko.tm.exception.field.IndexIncorrectException;
import ru.t1.zkovalenko.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected SqlSession getConnection() {
        return connectionService.openSession();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull SqlSession connection);

    @Override
    @NotNull
    public final String getOrderByField(@Nullable final Comparator comparator) {
        if (CreatedComparator.INSTANCE.equals(comparator)) return TableConstant.FIELD_CREATED;
        if (StatusComparator.INSTANCE.equals(comparator)) return TableConstant.FIELD_STATUS;
        if (NameComparator.INSTANCE.equals(comparator)) return TableConstant.FIELD_NAME;
        else return TableConstant.FIELD_ID;
    }

    @Override
    public void clear() {
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String sortField) {
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(sortField);
        }
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return findAll(getOrderByField(null));
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        return findAll(getOrderByField(comparator));
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        return findAll(getOrderByField(sort.getComparator()));
    }

    @NotNull
    @Override
    public M add(@Nullable M model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    public M update(@Nullable M model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.update(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull Collection<M> models) {
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            models.forEach(repository::add);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull Collection<M> models) {
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.clear();
            models.forEach(repository::add);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @Override
    public boolean existById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneById(id) != null;
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneById(id);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(index);
        }
    }

    @NotNull
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.remove(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

}
