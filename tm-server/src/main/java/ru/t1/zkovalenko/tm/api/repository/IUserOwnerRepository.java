package ru.t1.zkovalenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.AbstractUserOwnerModel;

import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    @NotNull
    List<M> findAllByUserId(@NotNull String userId, @NotNull String sortField);

    void clearByUserId(String userId);

    @Nullable
    M findOneByIdUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndexUserId(@NotNull String userId, @NotNull Integer index);

}
