package ru.t1.zkovalenko.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.api.repository.ISessionRepository;
import ru.t1.zkovalenko.tm.api.repository.IUserOwnerRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.ISessionService;
import ru.t1.zkovalenko.tm.model.Session;

public class SessionService extends AbstractUserOwnerService<Session> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull IUserOwnerRepository<Session> getRepository(@NotNull SqlSession connection) {
        return connection.getMapper(ISessionRepository.class);
    }

}
