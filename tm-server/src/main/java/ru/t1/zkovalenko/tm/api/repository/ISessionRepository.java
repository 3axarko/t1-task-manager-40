package ru.t1.zkovalenko.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IUserOwnerRepository<Session> {

    @Update("UPDATE tm_session " +
            "SET user_id = #{userId}, " +
            "   created = #{date}, " +
            "   role = #{role}, " +
            " WHERE id = #{id}")
    void update(@NotNull Session session);

    @Insert("INSERT INTO tm_session " +
            "(id, user_id, created, role) " +
            "VALUES (#{id}, #{userId}, #{date}, #{role})")
    void add(@NotNull Session session);

    @Select("SELECT * FROM tm_session order by #{sortField}")
    @Results({
            @Result(property = "date", column = "created"),
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<Session> findAll(@NotNull String sortField);

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} order by #{sortField}")
    @Results({
            @Result(property = "date", column = "created"),
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<Session> findAllByUserId(@Param("userId") @NotNull String userId, @Param("sortField") @NotNull String sortField);

    @Delete("DELETE FROM tm_session")
    void clear();

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clearByUserId(String userId);

    @Select("SELECT * FROM tm_session WHERE id = #{id} LIMIT 1")
    @Results({
            @Result(property = "date", column = "created"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneById(@NotNull String id);

    @Select("SELECT * from (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM tm_session) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "date", column = "created"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneByIndex(@NotNull Integer index);

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void remove(@NotNull Session session);

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    @Results({
            @Result(property = "date", column = "created"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneByIdUserId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);

    @Select("SELECT * FROM (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM tm_session WHERE user_id = #{userId}) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "date", column = "created"),
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneByIndexUserId(@Param("userId") @NotNull String userId,
                                           @Param("index") @NotNull Integer index);
}
