package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.enumerated.Sort;
import ru.t1.zkovalenko.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull String getOrderByField(@Nullable Comparator comparator);

    void clear();

    @NotNull List<M> findAll(@NotNull String sortField);

    @NotNull List<M> findAll();

    @NotNull List<M> findAll(@Nullable Comparator comparator);

    @NotNull List<M> findAll(@Nullable Sort sort);

    M add(M model);

    M update(M model);

    @NotNull Collection<M> add(@NotNull Collection<M> models);

    @NotNull Collection<M> set(@NotNull Collection<M> models);

    boolean existById(@Nullable String id);

    M findOneById(@Nullable String id);

    M findOneByIndex(@Nullable Integer index);

    M remove(M model);

}
