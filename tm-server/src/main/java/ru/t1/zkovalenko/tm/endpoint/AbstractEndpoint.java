package ru.t1.zkovalenko.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.service.IServiceLocator;
import ru.t1.zkovalenko.tm.dto.request.user.AbstractUserRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.exception.user.AccessDeniedException;
import ru.t1.zkovalenko.tm.model.Session;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @SneakyThrows
    protected Session check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

    @NotNull
    @SneakyThrows
    protected Session check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final Session session = serviceLocator.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

}
