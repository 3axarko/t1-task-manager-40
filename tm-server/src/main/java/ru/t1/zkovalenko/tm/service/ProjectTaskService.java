package ru.t1.zkovalenko.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IProjectTaskService;
import ru.t1.zkovalenko.tm.exception.entity.ProjectNotFoundException;
import ru.t1.zkovalenko.tm.exception.entity.TaskNotFoundException;
import ru.t1.zkovalenko.tm.exception.field.IndexIncorrectException;
import ru.t1.zkovalenko.tm.exception.field.ProjectEmptyException;
import ru.t1.zkovalenko.tm.exception.field.TaskEmptyException;
import ru.t1.zkovalenko.tm.exception.field.UserIdEmptyException;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final SqlSession connection) {
        return connection.getMapper(ITaskRepository.class);
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final SqlSession connection) {
        return connection.getMapper(IProjectRepository.class);
    }

    @Override
    @SneakyThrows
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskEmptyException();
        @NotNull SqlSession connection = connectionService.openSession();
        try {
            IProjectRepository projectRepository = getProjectRepository(connection);
            if (projectRepository.findOneByIdUserId(userId, projectId) == null) throw new ProjectNotFoundException();
            ITaskRepository taskRepository = getTaskRepository(connection);
            @Nullable final Task task = taskRepository.findOneByIdUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectEmptyException();
        @NotNull SqlSession connection = connectionService.openSession();
        try {
            IProjectRepository projectRepository = getProjectRepository(connection);
            @Nullable final Project project = projectRepository.findOneById(projectId);
            if (project == null) throw new ProjectNotFoundException();
            ITaskRepository taskRepository = getTaskRepository(connection);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, project.getId());
            tasks.forEach(taskRepository::remove);
            projectRepository.remove(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @NotNull SqlSession connection = connectionService.openSession();
        try {
            IProjectRepository projectRepository = getProjectRepository(connection);
            @Nullable Project project = projectRepository.findOneByIndex(index);
            if (project == null) throw new ProjectNotFoundException();
            ITaskRepository taskRepository = getTaskRepository(connection);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, project.getId());
            tasks.forEach(taskRepository::remove);
            projectRepository.remove(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskEmptyException();
        @NotNull SqlSession connection = connectionService.openSession();
        try {
            IProjectRepository projectRepository = getProjectRepository(connection);
            if (projectRepository.findOneByIdUserId(userId, projectId) == null) throw new ProjectNotFoundException();
            ITaskRepository taskRepository = getTaskRepository(connection);
            @Nullable final Task task = taskRepository.findOneByIdUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

}
