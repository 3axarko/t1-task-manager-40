package ru.t1.zkovalenko.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.IUserOwnerRepository;
import ru.t1.zkovalenko.tm.api.service.IConnectionService;
import ru.t1.zkovalenko.tm.api.service.IUserOwnerService;
import ru.t1.zkovalenko.tm.enumerated.Sort;
import ru.t1.zkovalenko.tm.exception.entity.EntityNotFoundException;
import ru.t1.zkovalenko.tm.exception.field.IdEmptyException;
import ru.t1.zkovalenko.tm.exception.field.IndexIncorrectException;
import ru.t1.zkovalenko.tm.exception.field.UserIdEmptyException;
import ru.t1.zkovalenko.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerService<M extends AbstractUserOwnerModel>
        extends AbstractService<M> implements IUserOwnerService<M> {

    public AbstractUserOwnerService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnerRepository<M> getRepository(@NotNull SqlSession connection);

    @NotNull
    private List<M> findAll(@Nullable final String userId, @NotNull String sortField) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            return repository.findAllByUserId(userId, sortField);
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession connection = getConnection();
        try {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            repository.clearByUserId(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return findAll(userId, getOrderByField(comparator));
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return findAll(userId, sort == null ? null : sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model;
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            model = repository.findOneByIdUserId(userId, id);
        }
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final M model;
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            model = repository.findOneByIndexUserId(userId, index);
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model;
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            model = repository.findOneByIdUserId(userId, id);
        }
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final M model;
        try (@NotNull final SqlSession connection = getConnection()) {
            @NotNull final IUserOwnerRepository<M> repository = getRepository(connection);
            model = repository.findOneByIndexUserId(userId, index);
        }
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

}
