package ru.t1.zkovalenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.task.TaskShowByIdRequest;
import ru.t1.zkovalenko.tm.model.Task;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Display task by id";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable final Task task = getTaskEndpoint().showByIdTask(request).getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
