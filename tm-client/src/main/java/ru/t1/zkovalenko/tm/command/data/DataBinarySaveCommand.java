package ru.t1.zkovalenko.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.dto.request.data.DataBinarySaveRequest;
import ru.t1.zkovalenko.tm.enumerated.Role;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-binary";

    @NotNull
    public static final String ARGUMENT = "-dsb";

    @NotNull
    public static final String DESCRIPTION = "Save data binary into file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(getToken());
        getDomainEndpoint().binarySaveData(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
